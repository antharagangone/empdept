package com.model;

public class Department {
	private int deptId;
	private String deptName;
	private String location;
	
	public Department(){
		super();
	}
	
	public Department(int deptId, String deptName, String location){
		super();
		this.deptId = deptId;
		this.deptName = deptName;
		this.location = location;
	}
	
	public int getDeptId(){
		return deptId;
	}
	public String getDeptName(){
		return deptName;
	}
	public String getLocation(){
		return location;
	}
	
	public void getDeptId(int deptId){
		this.deptId = deptId;
	}
	public void getDeptName(String deptName){
		this.deptName = deptName;
	}
	public void getLocation(String location){
		this.location = location;
	}
	
	@Override
	public String toString() {
		return "DepartmentDetails [deptId=" + deptId + ", deptName=" + deptName + ", location=" + location + "]";
	}
}
