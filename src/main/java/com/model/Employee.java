package com.model;

import java.util.Date;

public class Employee {
	private int empId;
	private String empName;
	private double salary;
	private String gender;
	private Date doj;
	private String country;
	private String emailId;
	private String password;
	
	public Employee(){
		super();
	}
	
	public Employee(int empId, String empName, double salary, String gender, Date doj, String country, String emailId, String password){
		super();
		this.empId = empId;
		this.empName = empName;
		this.salary = salary;
		this.gender = gender;
		this.doj = doj;
		this.country = country;
		this.emailId = emailId;
		this.password = password;
	}
	
	public int getEmpId(){
		return empId;
	}
	public String getEmpName(){
		return empName;
	}
	public double getSalary(){
		return salary;
	}
	public String getGender(){
		return gender;
	}
	public Date getDoj(){
		return doj;
	}
	public String getCountry(){
		return country;
	}
	public String getEmailId(){
		return emailId;
	}
	public String getPassword(){
		return password;
	}
	
	public void getEmpId(int empId){
		this.empId = empId;
	}
	public void getEmpName(String empName){
		this.empName = empName;
	}
	public void getSalary(double salary){
		this.salary = salary;
	}
	public void getGender(String gender){
		this.gender = gender;
	}
	public void getDoj(Date doj){
		this.doj = doj;
	}
	public void getCountry(String country){
		this.country = country;
	}
	public void getEmailId(String emailId){
		this.emailId = emailId;
	}
	public void getPassword(String password){
		this.password = password;
	}
	
	@Override
	public String toString() {
		return "EmployeeDetails [empId=" + empId + ", empName=" + empName + ", salary=" + salary + ", gender=" + gender + ", doj=" + doj + ", country=" + country + ", emailId=" + emailId + ", password=" + password + "]";
	}
}
